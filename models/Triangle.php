<?php

include_once('Shape.php');

class Triangle extends Shape
{

    private $a;

    private $b;

    private $c;

    function __construct($a, $b, $c) 
    {
        if(is_numeric($a) && is_numeric($b) && is_numeric($c)){ // a, b and c MUST be numerical
            if( $a + $b > $c && 
                $b + $c > $a && 
                $a + $c > $b){  //  The SUM of two sides must add up to be greather than the length of the remaining third side
                    
                $this->a = $a;
                $this->b = $b;
                $this->c = $c;

            }else{
                throw new Exception('Not triangle');
            }
        }else{
            throw new Exception('Arguments are not numerical');
        }
    }

    public function getA(): float
    {
        return $this->a;
    }

    public function setA(float $a): self
    {
        $this->a = $a;

        return $this;
    }

    public function getB(): float
    {
        return $this->b;
    }

    public function setB(float $b): self
    {
        $this->b = $b;

        return $this;
    }

    public function getC(): float
    {
        return $this->c;
    }

    public function setC(float $c): self
    {
        $this->c = $c;

        return $this;
    }

    public function getCircumference(): float   // Calculate and return circumference
    {
        return $this->a + $this->b + $this->c;
    }

    public function getSurface(): float     // Calculate and return circumference
    {
        $s = $this->getCircumference() / 2;   // Calculate semi-perimeter of Heron's formula

        $this->surface = sqrt($s * ($s - $this->a) * ($s - $this->b) * ($s - $this->c));
    }
}
