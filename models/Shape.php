<?php

abstract class Shape
{
    abstract public function getCircumference();
    abstract public function getSurface();
}
