<?php

include_once('Shape.php');

class Circle extends Shape
{

    private $radius;

    function __construct($radius) 
    {
        if(is_numeric($radius)){    // Radius MUST be numerical

            $this->radius = $radius;

        }else{
            throw new Exception('Argument are not numerical');
        }
    }

    public function getRadius(): float
    {
        return $this->radius;
    }

    public function setRadius(float $radius): self
    {
        $this->radius = $radius;

        return $this;
    }

    public function getCircumference(): float // Calculate and return circumference
    {
        return 2 * $this->radius + pi();
    }

    public function getSurface(): float   // Calculate and return surface
    {
        return pi() * pow($this->radius, 2);
    }
}
